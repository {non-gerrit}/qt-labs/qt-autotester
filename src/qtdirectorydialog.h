/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Autotester project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QTDIRECTORYDIALOG_H
#define QTDIRECTORYDIALOG_H

#include <QDialog>

namespace Ui {
    class QtDirectoryDialog;
}

class QLineEdit;
class QPushButton;
class QLabel;

class QtDirectoryDialog : public QDialog
{
    Q_OBJECT

public:
    explicit QtDirectoryDialog(QWidget *parent = 0);
    ~QtDirectoryDialog();

    void promptDialog();
    QString name() const;
    QString location() const;
#ifdef Q_WS_WIN
    QString makeLocation() const;
#endif

protected Q_SLOTS:
    void on_browse_clicked();
    void on_name_textChanged(const QString &text);
    void on_qmake_textChanged(const QString &text);
#ifdef Q_WS_WIN
    void onuimake_textChanged(const QString &text);
    void onuibrowsemake_clicked();
#endif

private:
    Ui::QtDirectoryDialog *ui;
    bool nameOk;
    bool qmakeOk;
#ifdef Q_WS_WIN
    bool makeOk;
    QString makespec;
    QString makename;
    QLineEdit *uimake;
    QPushButton *uibrowsemake;
    QLabel *uimakelabel;

    void updateMakeFields();
#endif

    void checkFields();
};

#endif // QTDIRECTORYDIALOG_H
