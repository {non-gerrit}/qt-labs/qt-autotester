/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Autotester project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "mainwindow.h"
#include "testview.h"
#include "testprofile.h"
#include "testgraphicsview.h"

#include <QPen>
#include <QFontMetricsF>
#include <QGraphicsDropShadowEffect>
#include <QGraphicsSceneContextMenuEvent>
#include <QMenu>
#include <QVariant>
#include <QGraphicsScene>
#include <QDebug>

/**************************************************************************************************
 **************************************************************************************************/
TestView::TestView(Test *test, QGraphicsItem *parent)
    : QGraphicsWidget(parent),
    t(test)
{
    setObjectName(test->name());
    setCacheMode(QGraphicsItem::ItemCoordinateCache);

    backItem = new QGraphicsPathItem;
    QPainterPath path;
    path.addRoundedRect(0, 0, ITEM_WIDTH, ITEM_HEIGHT, 15, 15);
    backItem->setPath(path);
    backItem->setPen(QPen(QColor(BORDER_COLOR_INIT), 2));
    backItem->setBrush(createGradientFromColor(COLOR_INIT));
    backItem->setParentItem(this);

    setVisible(false);
    resize(ITEM_WIDTH, ITEM_HEIGHT);
    setToolTip(test->name());

    setTransformOriginPoint(ITEM_WIDTH / 2.0, ITEM_HEIGHT / 2.0);

    QString testname = test->name();

    // Hardcore some softbreaks...
    if (testname.startsWith("qdeclarative"))
        testname = testname.left(12)+QChar(0x200B)+testname.mid(12);
    else if (testname.startsWith("qgraphics"))
        testname = testname.left(9)+QChar(0x200B)+testname.mid(9);
    else if (testname.startsWith("qscript"))
        testname = testname.left(7)+QChar(0x200B)+testname.mid(7);
    else if (testname.startsWith("qnetwork"))
        testname = testname.left(8)+QChar(0x200B)+testname.mid(8);

    title = new QGraphicsTextItem(testname, this);
    title->setTextInteractionFlags(Qt::NoTextInteraction);
    title->setDefaultTextColor(Qt::white);
    QFont f("Monotype", 9, QFont::DemiBold);
    QFontMetricsF fm(f);
    if (fm.width(test->name()) >= ITEM_WIDTH - 10) {
        title->setTextWidth(ITEM_WIDTH - 10);
    }
    title->setFont(f);
    title->setPos(geometry().width() / 2 - title->boundingRect().width() / 2, 2);

    QGraphicsDropShadowEffect *shadow = new QGraphicsDropShadowEffect;
    shadow->setBlurRadius(4);
    shadow->setOffset(2, 3);
    shadow->setColor(QColor(50, 50, 50));
    title->setGraphicsEffect(shadow);

    imgContainer = new QGraphicsWidget(this);
    img = new QGraphicsPixmapItem(imgContainer);
    img->setPos(0, 0);

    QFont f2("Monotype", 8/*, QFont::DemiBold*/);
    QFont f3("Monotype", 14, QFont::DemiBold);
    nbFail = new QGraphicsTextItem(imgContainer);
    nbFail->setTextInteractionFlags(Qt::NoTextInteraction);
    nbFail->setFont(f3);
    nbFail->setDefaultTextColor(Qt::white);
    message = new QGraphicsTextItem(this);
    message->setTextInteractionFlags(Qt::NoTextInteraction);
    message->setFont(f2);
    message->setDefaultTextColor(Qt::white);

    connect(t, SIGNAL(statusChanged(Test::TestStatus)), this, SLOT(onTestStatusChanged(Test::TestStatus)));
}

/**************************************************************************************************
 **************************************************************************************************/
TestView::~TestView()
{

}

/**************************************************************************************************
 **************************************************************************************************/
QLinearGradient TestView::createGradientFromColor(const QColor &c)
{
    QLinearGradient linearGrad(QPointF(backItem->boundingRect().width() * 0.42, 0),
                               QPointF(backItem->boundingRect().width() * 0.76, backItem->boundingRect().height()));
    QColor dark;
    dark.setCmyk(c.cyan(), c.magenta(), c.yellow(), 180);
    linearGrad.setColorAt(0, dark);
    linearGrad.setColorAt(1, c);
    return linearGrad;
}

/**************************************************************************************************
 **************************************************************************************************/
void TestView::onTestStatusChanged(Test::TestStatus status)
{
    QColor bc;
    QColor c;

    img->setOpacity(1);

    int failCount = t->result().failures.count();
    int warnCount = 0;
    foreach (const TestIncident &incident, t->result().messages) {
        if (incident.type == "qwarn")
            warnCount++;
    }
    nbFail->setPlainText("");
    nbFail->setVisible(false);

    switch (status) {
    case Test::Initial:
        bc = BORDER_COLOR_INIT;
        c = COLOR_INIT;
        img->setVisible(false);
        message->setVisible(false);
        break;
    case Test::Building:
        img->setPixmap(QPixmap(":/images/build.png").scaled(QSize(32, 32), Qt::KeepAspectRatio, Qt::SmoothTransformation));
        img->setVisible(true);
        message->setVisible(false);
        bc = BORDER_COLOR_RUN;
        c = COLOR_RUN;
        break;
    case Test::Running:
        img->setPixmap(QPixmap(":/images/gear.png").scaled(QSize(32, 32), Qt::KeepAspectRatio, Qt::SmoothTransformation));
        img->setVisible(true);
        message->setVisible(false);
        bc = BORDER_COLOR_RUN;
        c = COLOR_RUN;
        break;
    case Test::BuildError:
        img->setPixmap(QPixmap(":/images/warn.png"));
        img->setVisible(true);
        bc = BORDER_COLOR_FAIL;
        c = COLOR_FAIL;
        message->setPlainText("Build Error");
        message->setVisible(true);
        break;
    case Test::RunError:
        img->setPixmap(QPixmap(":/images/warn.png"));
        img->setVisible(true);
        message->setPlainText("Run Error");
        message->setVisible(true);
        bc = BORDER_COLOR_FAIL;
        c = COLOR_FAIL;
        break;
    case Test::TestFail:
    case Test::TestPass:
        if (failCount == 0 && t->result().messages.count() == 0) {
            img->setPixmap(QPixmap(":/images/check.png").scaled(QSize(32, 32), Qt::KeepAspectRatio, Qt::SmoothTransformation));
            img->setOpacity(0.5);
            message->setVisible(false);
        } else {
            img->setPixmap(QPixmap(":/images/warn.png"));
            nbFail->setPlainText(QString::number(failCount == 0 ? t->result().messages.count() : failCount));
            nbFail->setPos(img->boundingRect().width() + 2, -6);
            nbFail->setVisible(true);
            message->setPlainText(QLatin1String(failCount == 0 ?
                                                QLatin1String(t->result().messages.count() > 1 ? "messages" : "message")
                                                : QLatin1String(failCount > 1 ? "failures" : "failure")));
            message->setVisible(true);
        }
        img->setVisible(true);
        if (failCount > 0) {
            bc = BORDER_COLOR_FAIL;
            c = COLOR_FAIL;
        } else if (warnCount > 0) {
            bc = BORDER_COLOR_WARN;
            c = COLOR_WARN;
        } else {
            bc = BORDER_COLOR_PASS;
            c = COLOR_PASS;
        }
        break;
    case Test::Skipped:
        img->setVisible(false);
        message->setPlainText("Skipped");
        message->setVisible(true);
        bc = BORDER_COLOR_SKIP;
        c = COLOR_SKIP;
        break;
    }

    qreal imgContainerX = backItem->boundingRect().width() / 2 - img->boundingRect().width() / 2;
    qreal imgContainerY = backItem->boundingRect().height() / 2 - img->boundingRect().height() / 2 + 5;
    if (nbFail->isVisible()) {
        imgContainerX -= nbFail->boundingRect().width() / 2;
        imgContainerY -= 5;
    }
    imgContainer->setPos(imgContainerX, imgContainerY);
    imgContainer->setTransformOriginPoint(img->boundingRect().width() / 2, img->boundingRect().height() / 2);
    backItem->setPen(QPen(QColor(bc), 2));
    backItem->setBrush(createGradientFromColor(c));
    message->setPos(backItem->boundingRect().width() / 2 - message->boundingRect().width() / 2,
                    imgContainer->pos().y() + img->boundingRect().height() + 1);
}

/**************************************************************************************************
 **************************************************************************************************/
void TestView::moveEvent(QGraphicsSceneMoveEvent *event)
{
    QGraphicsWidget::moveEvent(event);

    if (!isVisible())
        setVisible(true);
}

/**************************************************************************************************
 **************************************************************************************************/
void TestView::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);

    if (event->button() != Qt::LeftButton) {
        QGraphicsWidget::mousePressEvent(event);
        return;
    }

    if (scene()->views().count() == 0)
        return;

    TestGraphicsView *view = dynamic_cast<TestGraphicsView *>(scene()->views().at(0));
    if (!view)
        return;

    if (t->result().failures.isEmpty() && t->result().messages.isEmpty())
        return;

    view->showTestResultsDetail(this);
}

/**************************************************************************************************
 **************************************************************************************************/
void TestView::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QMenu menu;

    QAction *a = menu.addAction(t->name());
    a->setEnabled(false);
    menu.addSeparator();
    if ((MainWindow::getCurrentProfile()->runningState() == TestProfile::NotRunning || t->runInParallel())
        && t->status() != Test::Building
        && t->status() != Test::Running) {
        a = menu.addAction(QIcon::fromTheme("media-playback-start", QIcon(QPixmap(":/images/start.png").scaled(14, 14, Qt::KeepAspectRatio, Qt::SmoothTransformation))), "Run");
        QObject::connect(a, SIGNAL(triggered()), this, SLOT(runTest()));
    }
    if (t->status() == Test::Building || t->status() == Test::Running) {
        a = menu.addAction(QIcon::fromTheme("media-playback-stop", QIcon(QPixmap(":/images/stop.png").scaled(14, 14, Qt::KeepAspectRatio, Qt::SmoothTransformation))), "Cancel");
        QObject::connect(a, SIGNAL(triggered()), this, SLOT(cancelTest()));
    }
    if (MainWindow::getCurrentProfile()->runningState() == TestProfile::NotRunning) {
        menu.addSeparator();
        a = menu.addAction(QIcon::fromTheme("edit-delete", QIcon(QPixmap(":/images/delete.png").scaled(12, 12))), "Remove");
        QObject::connect(a, SIGNAL(triggered()), this, SLOT(removeTest()));
    }

    menu.exec(event->screenPos());

    event->accept();
}

/**************************************************************************************************
 **************************************************************************************************/
void TestView::runTest()
{
    if ((MainWindow::getCurrentProfile()->runningState() == TestProfile::NotRunning || t->runInParallel())
        && t->status() != Test::Building
        && t->status() != Test::Running) {
        MainWindow::getCurrentProfile()->run(t);
    }
}

/**************************************************************************************************
 **************************************************************************************************/
void TestView::cancelTest()
{
    if (t->status() == Test::Building || t->status() == Test::Running)
        t->cancelRun();
}

/**************************************************************************************************
 **************************************************************************************************/
void TestView::removeTest()
{
    if (MainWindow::getCurrentProfile()->runningState() == TestProfile::NotRunning)
        MainWindow::removeTest(t->name());
}
