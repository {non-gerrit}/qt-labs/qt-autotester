/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Autotester project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TESTVIEW_H
#define TESTVIEW_H

#include <QGraphicsWidget>
#include "test.h"

#define ITEM_WIDTH 105
#define ITEM_HEIGHT 105

#define BORDER_COLOR_INIT "gray"
#define COLOR_INIT "darkgray"
#define BORDER_COLOR_PASS "olivedrab"
#define COLOR_PASS "yellowgreen"
#define BORDER_COLOR_WARN "#dddd00"
#define COLOR_WARN "yellow"
#define BORDER_COLOR_FAIL "indianred"
#define COLOR_FAIL "tomato"
#define BORDER_COLOR_SKIP "tan"
#define COLOR_SKIP "khaki"
#define BORDER_COLOR_RUN "floralwhite"
#define COLOR_RUN "white"

class TestView : public QGraphicsWidget
{
    Q_OBJECT

public:
    TestView(Test *test, QGraphicsItem * parent = 0);
    ~TestView();

    Test *test() const
    { return t; }
    QRectF boundingRect() const
    { return backItem->boundingRect(); }
    void setGeometry(const QRectF &rect)
    { QGraphicsWidget::setGeometry(QRectF(rect.topLeft(), backItem->boundingRect().size())); }

protected:
    QLinearGradient createGradientFromColor(const QColor &c);
    void moveEvent(QGraphicsSceneMoveEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

protected Q_SLOTS:
    void onTestStatusChanged(Test::TestStatus status);
    void runTest();
    void cancelTest();
    void removeTest();

private:
    Test *t;
    QGraphicsPathItem *backItem;
    QGraphicsTextItem *title;
    QGraphicsWidget *imgContainer;
    QGraphicsPixmapItem *img;
    QGraphicsTextItem *nbFail;
    QGraphicsTextItem *message;
};

#endif // TESTVIEW_H
