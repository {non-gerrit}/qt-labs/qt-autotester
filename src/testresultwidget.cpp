/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Autotester project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testresultwidget.h"
#include "ui_testresultwidget.h"
#include "test.h"
#include "testview.h"
#include <QDebug>

/**************************************************************************************************
 **************************************************************************************************/
TestResultWidget::TestResultWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestResultWidget)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_NoSystemBackground);
}

/**************************************************************************************************
 **************************************************************************************************/
TestResultWidget::~TestResultWidget()
{
    delete ui;
}

/**************************************************************************************************
 **************************************************************************************************/
void TestResultWidget::setTest(TestView *t)
{
    connect(t, SIGNAL(destroyed()), this, SLOT(onTestChanged()));
    connect(t->test(), SIGNAL(statusChanged(Test::TestStatus)), this, SLOT(onTestChanged()));
    test = t;

    ui->title->setText(t->test()->name());
    TestResult r = t->test()->result();
    QString incidentCount;
    if (r.failures.count() > 0)
        incidentCount = QString::number(r.failures.count()) + QLatin1String(" failed");
    if (r.messages.count() > 0) {
        incidentCount += QLatin1String(r.failures.count() > 0 ? ", " : "")
                         + QString::number(r.messages.count())
                         + QLatin1String(r.messages.count() > 1 ? " messages" : " message");
    }
    ui->failcount->setText(incidentCount);

    // Yes, this is the problem with using style sheets...
    ui->frame->setStyleSheet(
        r.pass ? "QFrame#frame {\nbackground-color: qlineargradient(spread:pad, x1:0.42, y1:0, x2:0.76, y2:1, stop:0 rgba(75, 75, 20, 255), stop:1 rgba(255, 255, 71, 255));\nborder: 2px solid rgba(205, 205, 92, 255);\nborder-radius: 20px;\n}"
               : "QFrame#frame {\nbackground-color: qlineargradient(spread:pad, x1:0.42, y1:0, x2:0.76, y2:1, stop:0 rgba(75, 29, 20, 255), stop:1 rgba(255, 99, 71, 255));\nborder: 2px solid rgba(205, 92, 92, 255);\nborder-radius: 20px;\n}"
    );

    ui->listWidget->clear();
    listSize = QSize(0, 0);
    QFontMetrics f(ui->listWidget->font());
    QPixmap pix(":/images/error.png");

    QList<TestIncident> allIncidents = r.failures + r.messages;
    foreach (const TestIncident &incident, allIncidents) {
        QString firstL = incident.type.toUpper() + ": " + incident.function + "(" + incident.data + ")";
        QString secondL = incident.message;
        QString thirdL;
        if (!incident.file.isEmpty() || incident.line)
            thirdL = "Loc: [" + incident.file + "(" + QString::number(incident.line) + ")]";
        QIcon ico(":/images/error.png");
        ui->listWidget->addItem(new QListWidgetItem(ico, firstL.toLatin1()));
        ui->listWidget->addItem(secondL.toLatin1());
        if (!thirdL.isEmpty())
            ui->listWidget->addItem(thirdL.toLatin1());
        ui->listWidget->addItem("");

        int lines = qMax(secondL.count('\n'), qMax(secondL.count("\r\n"), secondL.count('\r'))) + 1;
        int height = qMax(pix.height(), f.height()) + (lines + (thirdL.isEmpty() ? 1 : 2)) * f.height();
        int maxWidth = qMax(f.width(pix.width() + firstL), qMax(f.width(secondL), f.width(thirdL)));
        listSize.setHeight(listSize.height() + height);
        listSize.setWidth(qMax(listSize.width(), maxWidth));
    }
}

/**************************************************************************************************
 **************************************************************************************************/
QListWidget *TestResultWidget::listWidget() const
{
    return ui->listWidget;
}

/**************************************************************************************************
 **************************************************************************************************/
QFrame *TestResultWidget::frame() const
{
    return ui->frame;
}

/**************************************************************************************************
 **************************************************************************************************/
QLabel *TestResultWidget::title() const
{
    return ui->title;
}

/**************************************************************************************************
 **************************************************************************************************/
QLabel *TestResultWidget::failCount() const
{
    return ui->failcount;
}

/**************************************************************************************************
 **************************************************************************************************/
void TestResultWidget::mouseReleaseEvent(QMouseEvent *e)
{
    Q_UNUSED(e);

    emit clicked();
}

/**************************************************************************************************
 **************************************************************************************************/
void TestResultWidget::onTestChanged()
{
    disconnect(test, SIGNAL(destroyed()), this, SLOT(onTestChanged()));
    disconnect(test->test(), SIGNAL(statusChanged(Test::TestStatus)), this, SLOT(onTestChanged()));

    emit clicked();
}
