/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Autotester project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "testgraphicsview.h"
#include "testviewlayout.h"
#include "testview.h"
#include "testresultwidget.h"
#include "test.h"
#include <math.h>

#include <QGraphicsWidget>
#include <QGraphicsDropShadowEffect>
#include <QStateMachine>
#include <QSignalTransition>
#include <QListWidget>
#include <QLayout>
#include <QLabel>
#include <QPropertyAnimation>
#include <QDebug>

/**************************************************************************************************
 **************************************************************************************************/
TestGraphicsView::TestGraphicsView(QWidget *parent)
    : QGraphicsView(parent),
    currentZoomLevel(1.0)
{
    setOptimizationFlag(QGraphicsView::DontSavePainterState);
    setCacheMode(QGraphicsView::CacheBackground);
    setAlignment(Qt::AlignTop | Qt::AlignLeft);
    setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    setScene(new QGraphicsScene);

    testsContainer = new QGraphicsWidget;
    testsContainer->setCacheMode(QGraphicsItem::DeviceCoordinateCache);
    layout = new TestViewLayout(TestViewLayout::Vertical, 1);
    layout->setScene(scene());
    layout->setHorizontalSpacing(ITEM_WIDTH + 15);
    layout->setVerticalSpacing(ITEM_HEIGHT + 15);
    layout->setContentsMargins(20, 20, 20, 20);
    testsContainer->setLayout(layout);
    scene()->addItem(testsContainer);

    widget = new TestResultWidget(this);
    widget->setVisible(false);
    QGraphicsDropShadowEffect *shad = new QGraphicsDropShadowEffect;
    shad->setBlurRadius(10);
    shad->setColor(QColor(50, 50, 50));
    widget->setGraphicsEffect(shad);

    machine = new QStateMachine;
    detailsVisible = new QState;
    detailsVisible->assignProperty(testsContainer, "opacity", 0.4);
    detailsVisible->assignProperty(widget, "visible", true);

    detailsNotVisible = new QState;
    detailsNotVisible->assignProperty(testsContainer, "opacity", 1);
    detailsNotVisible->assignProperty(widget, "visible", false);

    QSignalTransition *transition1 = detailsVisible->addTransition(widget, SIGNAL(clicked()), detailsNotVisible);
    QSignalTransition *transition2 = detailsNotVisible->addTransition(this, SIGNAL(detailsRequested()), detailsVisible);
    transition1->addAnimation(new QPropertyAnimation(testsContainer, "opacity"));
    transition2->addAnimation(new QPropertyAnimation(testsContainer, "opacity"));

    machine->addState(detailsVisible);
    machine->addState(detailsNotVisible);
    machine->setInitialState(detailsNotVisible);
    machine->start();
}

/**************************************************************************************************
 **************************************************************************************************/
TestGraphicsView::~TestGraphicsView()
{
    delete machine;
}

/**************************************************************************************************
 **************************************************************************************************/
void TestGraphicsView::wheelEvent(QWheelEvent *event)
{
    if (event->modifiers() & Qt::ControlModifier) {
        if (event->delta() > 0)
            zoomIn();
        else
            zoomOut();
    } else {
        QGraphicsView::wheelEvent(event);
    }
}

/**************************************************************************************************
 **************************************************************************************************/
void TestGraphicsView::resizeEvent(QResizeEvent *event)
{
    QGraphicsView::resizeEvent(event);

    updateLayout();
}

/**************************************************************************************************
 **************************************************************************************************/
void TestGraphicsView::updateLayout()
{
    QSizeF effectiveSize = mapToScene(rect()).boundingRect().size();

    int length;
    if (layout->flowDirection() == TestViewLayout::Vertical)
        length = (effectiveSize.width() - 40) / layout->horizontalSpacing();
    else
        length = (effectiveSize.height() - 40) / layout->verticalSpacing();
    layout->setLayoutLength(length);

    QLinearGradient grad(0, 0, 0, effectiveSize.height());
    grad.setColorAt(0, Qt::black);
    grad.setColorAt(0.49, "dimgray");
    grad.setColorAt(0.51, "dimgray");
    grad.setColorAt(0.55, QColor(80, 80, 80));
    grad.setColorAt(1, Qt::black);
    setBackgroundBrush(grad);

    if (widget->isVisible())
        adjustDetailsWidget();
}

/**************************************************************************************************
 **************************************************************************************************/
void TestGraphicsView::showTestResultsDetail(TestView *t)
{
    widget->setTest(t);
    adjustDetailsWidget();

    emit detailsRequested();
}

/**************************************************************************************************
 **************************************************************************************************/
void TestGraphicsView::adjustDetailsWidget()
{
    QSize contSize = widget->contentsSize();
    QMargins margins = widget->frame()->layout()->contentsMargins();
    int w = margins.left() + contSize.width() + margins.right() + 45;
    int h = margins.top()
                 + widget->title()->height()
                 + widget->failCount()->height()
                 + widget->frame()->layout()->spacing() * 2
                 + contSize.height()
                 + margins.bottom() + 15;
    w = qMin(width() * 0.8, qreal(w));
    h = qMin(height() * 0.7, qreal(h));
    widget->resize(w, h);

    widget->move(width() / 2 - widget->width() / 2,
                  height() / 2 - widget->height() / 2);
}

/**************************************************************************************************
 **************************************************************************************************/
void TestGraphicsView::zoomIn()
{
    setZoom(currentZoomLevel * ZOOM_IN_FACTOR);
}

/**************************************************************************************************
 **************************************************************************************************/
void TestGraphicsView::zoomOut()
{
    setZoom(currentZoomLevel * ZOOM_OUT_FACTOR);
}

/**************************************************************************************************
 **************************************************************************************************/
void TestGraphicsView::resetZoom()
{
    setZoom(1.0);
}

/**************************************************************************************************
 **************************************************************************************************/
void TestGraphicsView::zoomFit()
{
    qreal ratio = qreal(width()) / qreal(height());
    int rows = ::sqrt(layout->count() / ratio) + 1;

    qreal fitFactor = height() / layout->verticalSpacing() / rows;
    qreal margin = 40 * fitFactor;
    fitFactor = (height() - margin) / layout->verticalSpacing() / rows;

    fitFactor = qMax(qMin(fitFactor, ZOOM_MAX), ZOOM_MIN);
    setZoom(fitFactor);
}

/**************************************************************************************************
 **************************************************************************************************/
void TestGraphicsView::setZoom(qreal z)
{
    if (currentZoomLevel == z || z > ZOOM_MAX || z < ZOOM_MIN)
        return;

    currentZoomLevel = z;
    setTransform(QTransform().scale(currentZoomLevel, currentZoomLevel));
    updateLayout();
    emit zoomLevelChanged(currentZoomLevel);
}
