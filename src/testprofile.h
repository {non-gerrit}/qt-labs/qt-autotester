/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Autotester project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TESTPROFILE_H
#define TESTPROFILE_H

#include <QObject>
#include <QHash>
#include <QMap>
#include <QSet>
#include <QStringList>
#include "test.h"

class QtDirectory;

class TestProfile : public QObject
{
    Q_OBJECT

public:
    enum ProfileState {
        Saved,
        Unsaved
    };

    enum RunningState {
        NotRunning,
        Running
    };

    explicit TestProfile(const QString &name = QString());
    ~TestProfile();

    inline QString name() const
    { return profileName; }
    inline void setName(const QString& name)
    { profileName = name; }
    inline ProfileState state() const
    { return profileState; }
    inline QStringList testNames() const
    { return (tests.keys().toSet() + unsavedAddedTests.keys().toSet() - unsavedRemovedTests.toSet()).toList(); }
    inline int testCount() const
    { return tests.count() + unsavedAddedTests.count() - unsavedRemovedTests.count(); }
    inline QList<Test*> savedTests() const
    { return tests.values(); }
    inline void addTestToList(Test *test)
    {
        tests.insert(test->name(), test);
    }
    inline TestProfile::RunningState runningState() const
    { return runState; }
    inline Test *test(const QString &name) const
    {
        Test *t = tests.value(name);
        if (t)
            return t;
        else
            return unsavedAddedTests.value(name);
    }
    inline QString variant() const
    { return buildVariant; }

    void setVariant(const QString &var);

    void addTest(Test *test);
    void removeTest(const QString &testName);
    void restore();

    void run();
    void run(Test *);
    void cancelRun(bool cancelOnlyCurrentTest = false);
    void save();

Q_SIGNALS:
    void stateChanged(TestProfile::ProfileState newState);
    void runningStateChanged();

protected Q_SLOTS:
    void onRunningTestStatusChanged(Test::TestStatus status);

protected:
    void runNextTest();

private:
    QString profileName;
    QHash<QString, Test*> tests;
    QHash<QString, Test*> unsavedAddedTests;
    QStringList unsavedRemovedTests;
    ProfileState profileState;
    RunningState runState;

    QMap<QString, Test*> testsToRun;
    QMap<QString, Test*> parallelTestsToRun;

    QString buildVariant;
    int currentTestToRun;
    int currentParallelTestToRun;
    int runningTestCount;

    bool stopRun;
};

#endif // TESTPROFILE_H
