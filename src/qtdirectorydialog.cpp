/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Autotester project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qtdirectorydialog.h"
#include "ui_qtdirectorydialog.h"

#include <QFileDialog>
#include <QProcess>
#include <QDebug>

/**************************************************************************************************
 **************************************************************************************************/
QtDirectoryDialog::QtDirectoryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QtDirectoryDialog),
    qmakeOk(false)
{
    ui->setupUi(this);

#ifdef Q_WS_WIN
    uimakelabel = new QLabel;
    uimakelabel->setObjectName(QString::fromUtf8("uimakelabel"));
    uimake = new QLineEdit;
    uimake->setObjectName(QString::fromUtf8("uimake"));
    uibrowsemake = new QPushButton("Browse...");
    uibrowsemake->setObjectName(QString::fromUtf8("uibrowsemake"));

    connect(uimake, SIGNAL(textChanged(QString)), this, SLOT(onuimake_textChanged(QString)));
    connect(uibrowsemake, SIGNAL(clicked()), this, SLOT(onuibrowsemake_clicked()));
#endif
}

/**************************************************************************************************
 **************************************************************************************************/
QtDirectoryDialog::~QtDirectoryDialog()
{
    delete ui;
#ifdef Q_WS_WIN
    delete uimakelabel;
    delete uimake;
    delete uibrowsemake;
#endif
}

/**************************************************************************************************
 **************************************************************************************************/
QString QtDirectoryDialog::name() const
{
    return ui->name->text();
}

/**************************************************************************************************
 **************************************************************************************************/
QString QtDirectoryDialog::location() const
{
    return ui->qmake->text();
}

/**************************************************************************************************
 **************************************************************************************************/
#ifdef Q_WS_WIN
QString QtDirectoryDialog::makeLocation() const
{
    return uimake->text();
}
#endif

/**************************************************************************************************
 **************************************************************************************************/
void QtDirectoryDialog::promptDialog()
{
    ui->name->setText(QString());
    ui->qmake->setText(QString());
    ui->message->setText(QString());
    ui->name->setFocus();
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);

    show();
}

/**************************************************************************************************
 **************************************************************************************************/
void QtDirectoryDialog::on_browse_clicked()
{
    QString qmakeLoc = QFileDialog::getOpenFileName(this, "Select qmake location");
    if (!qmakeLoc.isEmpty())
        ui->qmake->setText(qmakeLoc);
}

/**************************************************************************************************
 **************************************************************************************************/
#ifdef Q_WS_WIN
void QtDirectoryDialog::onuibrowsemake_clicked()
{
    QString makeLoc = QFileDialog::getOpenFileName(this, "Select " + makename + " location");
    if (!makeLoc.isEmpty())
        uimake->setText(makeLoc);
}
#endif

/**************************************************************************************************
 **************************************************************************************************/
void QtDirectoryDialog::on_name_textChanged(const QString &text)
{
    Q_UNUSED(text)
    checkFields();
}

/**************************************************************************************************
 **************************************************************************************************/
void QtDirectoryDialog::on_qmake_textChanged(const QString &text)
{
    Q_UNUSED(text)
    checkFields();
}

/**************************************************************************************************
 **************************************************************************************************/
#ifdef Q_WS_WIN
void QtDirectoryDialog::onuimake_textChanged(const QString &text)
{
    Q_UNUSED(text)
    checkFields();
}
#endif

/**************************************************************************************************
 **************************************************************************************************/
void QtDirectoryDialog::checkFields()
{
    QString name = ui->name->text();
    QString qmake = ui->qmake->text();

    ui->message->setStyleSheet("color:red");
    if (name.isEmpty()) {
        ui->message->setText("Please enter a Qt version name.");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        return;
    }

    if (qmake.isEmpty()) {
        ui->message->setText("Please select a qmake location");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        qmakeOk = false;
#ifdef Q_WS_WIN
        updateMakeFields();
#endif
        return;
    }

    QFileInfo file(qmake);
    if (!file.exists() || !file.fileName().startsWith("qmake", Qt::CaseInsensitive)) {
        ui->message->setText("Unable to find qmake at this location");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        qmakeOk = false;
#ifdef Q_WS_WIN
        updateMakeFields();
#endif
        return;
    }

    QStringList arguments;
    arguments << "--version";
    QProcess process;
    process.start(qmake, arguments);
    if (!process.waitForStarted(2000)) {
        ui->message->setText("Incorrect qmake executable");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        qmakeOk = false;
#ifdef Q_WS_WIN
        updateMakeFields();
#endif
        return;
    }
    process.waitForFinished(1000);
    QString read = process.readAll();
    if (!read.startsWith("qmake version", Qt::CaseInsensitive)) {
        ui->message->setText("Incorrect qmake executable");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        qmakeOk = false;
#ifdef Q_WS_WIN
        updateMakeFields();
#endif
        return;
    }

    QString testsDir = QDir::fromNativeSeparators(qmake);
    QString toRemove = "/bin/qmake";
#ifdef Q_WS_WIN
    toRemove += QString(".exe");
#endif
    testsDir.replace(toRemove, QString("/tests"));
    QFileInfo dir(testsDir);
    if (!dir.exists() || !dir.isDir()) {
        ui->message->setText("Unable to find tests directory in this Qt version");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        qmakeOk = false;
#ifdef Q_WS_WIN
        updateMakeFields();
#endif
        return;
    }

#ifdef Q_WS_WIN
    makespec = "";
    QString qtdir = qmake;
    qtdir.replace("/bin/qmake.exe", "");
    QFile cache(qtdir + "/.qmake.cache");
    if (cache.open(QIODevice::ReadOnly | QIODevice::Text)) {
        while (!cache.atEnd()) {
            QString line = cache.readLine();
            if (line.contains("QMAKESPEC")) {
                if (line.contains("msvc")) {
                    makespec = "msvc";
                    makename = "nmake";
                } else if (line.contains("g++")) {
                    makespec = "g++";
                    makename = "mingw32-make";
                } else if (line.contains("symbian")) {
                    makespec = "symbian";
                    makename = "mingw32-make";
                }
                break;
            }
        }
    }
    if (makespec.isEmpty()) {
        ui->message->setText("Unsupported compiler used with this Qt version");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        qmakeOk = false;
        updateMakeFields();
        return;
    }
#endif

    qmakeOk = true;

#ifdef Q_WS_WIN
    updateMakeFields();

    if (uimake->text().isEmpty()) {
        ui->message->setText("Please select " + makename + " location");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        return;
    }

    QFileInfo filemake(uimake->text());
    if (!filemake.exists() || !filemake.fileName().startsWith(makename, Qt::CaseInsensitive)) {
        ui->message->setText("Unable to find " + makename + " at this location");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
        return;
    }
#endif

    QString version = read.mid(read.indexOf("Using ") + 6, 16);
    ui->message->setStyleSheet("color:green");
    ui->message->setText(QString("Found %1").arg(version));
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
}

/**************************************************************************************************
 **************************************************************************************************/
#ifdef Q_WS_WIN
void QtDirectoryDialog::updateMakeFields()
{
    if (qmakeOk) {
        if (!ui->gridLayout->itemAtPosition(2, 0)) {
            ui->gridLayout->addWidget(uimakelabel, 2, 0, 1, 1);
            ui->gridLayout->addWidget(uimake, 2, 1, 1, 1);
            ui->gridLayout->addWidget(uibrowsemake, 2, 2, 1, 1);
        }
        if (makespec == "msvc")
            uimakelabel->setText("nmake Location");
        else
            uimakelabel->setText("MinGW make Location");
    } else {
        if (ui->gridLayout->itemAtPosition(2, 0)) {
            ui->gridLayout->removeWidget(uimakelabel);
            ui->gridLayout->removeWidget(uimake);
            ui->gridLayout->removeWidget(uibrowsemake);
            uimakelabel->setParent(0);
            uimake->setParent(0);
            uibrowsemake->setParent(0);
        }
    }
}
#endif
