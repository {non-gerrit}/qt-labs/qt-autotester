/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Autotester project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHash>
#include <QSortFilterProxyModel>
#include <QMap>
#include <QTime>
#include "testprofile.h"

class QStandardItemModel;
class QStandardItem;
class QtDirectory;
class QtDirectoryDialog;
class TestView;
class TestGraphicsView;

namespace Ui {
    class MainWindow;
}

class CustomProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    CustomProxyModel() : QSortFilterProxyModel()
    {}
    ~CustomProxyModel()
    {}
    bool filterAcceptsIndex(const QModelIndex index,
                            const QModelIndex parent,
                            bool skipParentCheck = false,
                            bool skipChildrenCheck = false) const;

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
};

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    static QtDirectory *getCurrentQtVersion()
    { return currentRepository; }
    static TestProfile *getCurrentProfile()
    { return currentProfile; }
    static void removeTest(const QString &name);

protected Q_SLOTS:
    void onModelItemChanged(QStandardItem *item);
    void on_addVersion_clicked();
    void on_remVersion_clicked();
    void onDialogAccepted();
    void on_qtversions_currentIndexChanged(const QString &text);
    void on_qtvariants_currentIndexChanged(const QString &text);
    void on_profiles_currentIndexChanged(int index);
    void onProfileStateChanged(TestProfile::ProfileState newState);
    void onProfileRunningStateChanged();
    void on_reloadProfile_clicked();
    void on_remProfile_clicked();
    void on_saveProfile_clicked();
    void on_runTests_clicked();
    void on_runTestsNoQMake_clicked();
    void on_stopTests_clicked();
    void on_skipCurrent_clicked();
    void on_filter_textChanged(const QString & text);
    void onZoomLevelChanged(qreal zoomLevel);
    void on_actionSave_Results_triggered();
    void on_actionQuit_triggered();
    void on_actionZoom_to_Fit_triggered();
    void on_actionDefault_Zoom_triggered();
    void on_actionZoom_In_triggered();
    void on_actionZoom_Out_triggered();
    void on_actionRun_Tests_triggered();
    void on_actionRun_Tests_Skip_QMake_triggered();
    void on_actionStop_Tests_triggered();
    void on_actionSkip_Current_Test_triggered();
    void on_actionAbout_Autotester_triggered();
    void on_arguments_textChanged(const QString &text);
    void on_actionShow_Explorer_triggered();
    void on_actionShow_Run_Panel_triggered();
    void on_actionShow_Output_triggered();
    void onNewLogReceived(const QString &log);

protected:
    void closeEvent(QCloseEvent *);
    void timerEvent(QTimerEvent *);

private:
    void setCurrentQtRepo(const QString &repoName);
    void setCurrentProfile(const QString &profileName);
    void saveRepoSettings();
    void saveProfileSettings();
    void saveWindowSettings();
    void restoreSettings();
    void restoreWindowSettings();
    void addTest(const QString &name, const QString &path, const QString &proFile);
    void addProfile(TestProfile *profile);
    void removeProfile(const QString &name);

    Ui::MainWindow *ui;
    static TestGraphicsView *view;
    QtDirectoryDialog *dialog;
    static QMap<QString, TestView*> testviews;

    QHash<QString, QtDirectory*> repositories;
    QHash<QString, TestProfile*> profiles;
    static QStandardItemModel *currentAutoTestModel;
    CustomProxyModel *proxyModel;
    static QtDirectory *currentRepository;
    static TestProfile *currentProfile;
    bool inSetProfile;
    bool alreadyRestored;
    int timerID;

    int elapsedTimerID;
    QTime elapsedTimer;
};

#endif // MAINWINDOW_H
