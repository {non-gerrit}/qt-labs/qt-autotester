/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Autotester project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TESTVIEWLAYOUT_H
#define TESTVIEWLAYOUT_H

#include <QGraphicsGridLayout>

class TestView;

class TestViewLayout : public QGraphicsGridLayout
{
public:
    enum FlowDirection {
        Horizontal,
        Vertical
    };

    TestViewLayout(TestViewLayout::FlowDirection d, int l);
    ~TestViewLayout();

    TestViewLayout::FlowDirection flowDirection() const
    { return direction; }
    int layoutLength() const
    { return layoutLength(); }

    void setFlowDirection(TestViewLayout::FlowDirection d);
    void setLayoutLength(int l);
    void addItem(TestView *item);
    void removeTestView(TestView *item);

    int count() const
    { return QGraphicsGridLayout::count(); }
    void invalidate()
    { QGraphicsGridLayout::invalidate(); }
    void removeItem(QGraphicsLayoutItem *item);
    void relayout();
    QGraphicsLayoutItem *itemAt(int index) const
    { return QGraphicsGridLayout::itemAt(index); }
    QGraphicsLayoutItem *itemAt(int row, int column) const
    { return QGraphicsGridLayout::itemAt(row, column); }
    void removeAt(int index)
    { QGraphicsGridLayout::removeAt(index); }

    void clearItems()
    { items.clear(); }

    void setScene(QGraphicsScene *s)
    { scene = s; }


private:
    void adjustSceneRect();

    FlowDirection direction;
    int layLength;
    QMap<QString, TestView *> items;
    QGraphicsScene *scene;
};

#endif // TESTVIEWLAYOUT_H
