/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Autotester project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TEST_H
#define TEST_H

#include <QtCore/QObject>
#include <QtCore/QStringList>
#include <QtCore/QString>
#include <QtCore/QMetaType>
#include <QHash>

class QProcess;

struct TestIncident
{
    QString function;
    QString type;
    QString data;
    QString message;
    QString file;
    int line;
};

struct TestResult
{
    bool pass;
    QList<TestIncident> failures;
    QList<TestIncident> messages;
};

class Test : public QObject
{
    Q_OBJECT

public:
    enum TestStatus {
        Initial,
        Building,
        BuildError,
        Running,
        RunError,
        TestPass,
        TestFail,
        Skipped
    };
    Q_ENUMS(TestStatus)

    Test(const QString& name, const QString& path, const QString &pro);
    ~Test();

    inline QString name() const
    { return testName; }
    inline QString path() const
    { return testPath; }
    QString executable() const;
    inline Test::TestStatus status() const
    { return teststatus; }
    inline void reset()
    {
        setStatus(Initial);
        output.clear();
    }
    TestResult result() const
    { return r; }
    inline QString proFile() const
    { return testPro; }
    bool runInParallel() const;

    void run();
    void cancelRun();

    inline static void setRunQMake(bool r)
    { runQMake = r; }
    static void setGlobalArguments(const QString &args)
    { m_globalArguments = args; }
    static QString globalArguments()
    { return m_globalArguments; }

    inline bool isAvailable() const
    { return testIsAvailable; }
    inline void setAvailable(bool a)
    { testIsAvailable = a; }

Q_SIGNALS:
    void statusChanged(Test::TestStatus status);

protected:
    int buildTest();
    int runTest();
    int runExecutable(const QString &workdir,
                      const QString &executable,
                      const QString &env,
                      const QStringList &arguments,
                      bool ignoreErrorChannel = false);
    void setStatus(Test::TestStatus st);
    void processResult(const QString &result);
    QString makePrintable(const QString &out);

protected Q_SLOTS:
    void onProcessReadyRead();

private:
    QString testName;
    QString testPath;
    TestStatus teststatus;
    TestResult r;
    QProcess *p;
    QString testPro;
    static bool runQMake;
    static QString m_globalArguments;
    bool testIsAvailable;
    QString output;
};

Q_DECLARE_METATYPE(Test::TestStatus);

#endif // TEST_H
