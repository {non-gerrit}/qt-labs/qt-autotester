QT       += core gui xml

TARGET = autotester
TEMPLATE = app
DESTDIR = ../bin

SOURCES += main.cpp \
    mainwindow.cpp \
    test.cpp \
    testprofile.cpp \
    qtdirectory.cpp \
    qtdirectorydialog.cpp \
    testview.cpp \
    testviewlayout.cpp \
    testgraphicsview.cpp \
    testresultwidget.cpp \
    clearablelineedit.cpp \
    logger.cpp

HEADERS  += \
    mainwindow.h \
    test.h \
    testprofile.h \
    qtdirectory.h \
    qtdirectorydialog.h \
    testview.h \
    testviewlayout.h \
    testgraphicsview.h \
    testresultwidget.h \
    clearablelineedit.h \
    logger.h

FORMS += \
    mainwindow.ui \
    qtdirectorydialog.ui \
    testresultwidget.ui

RESOURCES += \
    images.qrc

MOC_DIR = ./.moc
OBJECTS_DIR = ./.obj
UI_DIR = ./.ui

macx {
    ICON = images/autotester.icns
}
