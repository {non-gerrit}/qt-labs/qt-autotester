/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Autotester project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qtdirectory.h"

#include <QProcess>
#include <QFile>
#include <QDir>
#include <QStandardItemModel>
#include <QDebug>

/**************************************************************************************************
 **************************************************************************************************/
QtDirectory::QtDirectory(const QString &name, const QString &location
#ifdef Q_WS_WIN
                        , const QString &make
#endif
                )
    : QObject(),
    repoName(name),
    repoQmake(location)
{
    repoBuildDir = location;
    QString toRemove = "/bin/qmake";
#ifdef Q_WS_WIN
    toRemove += QString(".exe");
#endif
    repoBuildDir.replace(toRemove, QString(""));
    repoTestBuildDir = repoBuildDir + QString("/tests/auto");
    repoBinDir = repoBuildDir + QString("/bin");

    bool symbianBuildSystem = false;
    repoVariants.clear();

    QFile cache(repoBuildDir + "/.qmake.cache");
    if (cache.open(QIODevice::ReadOnly | QIODevice::Text)) {
        while (!cache.atEnd()) {
            QString line = cache.readLine();
            if (line.contains("QT_SOURCE_TREE")) {
                line.replace("\\", "/");
                repoSrcDir = line;
                repoSrcDir.remove(0, repoSrcDir.indexOf("=") + 1);
                repoSrcDir = repoSrcDir.trimmed();
                repoSrcDir.replace("$$quote(", "");
                repoSrcDir.replace(")", "");
                repoTestSrcDir = repoSrcDir + QString("/tests/auto");
                break;
            }
            else if (line.startsWith("QMAKESPEC")) {
                if (line.contains("symbian")) {
                    repoRemoteOS = "symbian";
                }
                else if (line.contains("wince")) {
                    repoRemoteOS = "wince";
                }
                if (line.contains("symbian-abld") || line.contains("symbian-sbsv2")) {
                    symbianBuildSystem = true;
                    repoVariants.clear();
                    repoVariants << "release-armv6";
                    repoVariants << "release-armv5";
                    repoVariants << "release-gcce";
                    repoVariants << "release-winscw";
                    repoVariants << "debug-armv6";
                    repoVariants << "debug-armv5";
                    repoVariants << "debug-gcce";
                    repoVariants << "debug-winscw";
                }
            }
            else if (line.startsWith("CONFIG")) {
                if (!symbianBuildSystem) {
                    if(line.contains(" release "))
                        repoVariants << "release";
                    if(line.contains(" debug "))
                        repoVariants << "debug";
                }
                repoCrossCompiled = line.contains("cross_compile");
            }
        }
    }

    QStringList arguments;
    arguments << "--version";
    QProcess process;
    process.start(location, arguments);
    if (!process.waitForStarted(2000)) {
        return;
    }
    process.waitForFinished(1000);
    QString read = process.readAll();
    repoLibDir = read.split(" in ").at(1).trimmed();

#ifdef Q_WS_WIN
    repoMakeLocation = make;
    repoMakeDir = make;
    repoMakeDir.truncate(repoMakeDir.lastIndexOf("/"));
#endif
}

/**************************************************************************************************
 **************************************************************************************************/
QtDirectory::~QtDirectory()
{
}

/**************************************************************************************************
 **************************************************************************************************/
QStandardItemModel *QtDirectory::createModelFromPro() const
{
    QStandardItemModel *model = new QStandardItemModel;
    model->setObjectName(repoBuildDir);
    model->setHorizontalHeaderItem(0, new QStandardItem("Auto-tests"));

    QStandardItem *invisibleItem = model->invisibleRootItem();
    QStandardItem *rootItem = new QStandardItem("All Tests");
    rootItem->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
    rootItem->setData(Qt::Unchecked, Qt::CheckStateRole);
    rootItem->setIcon(QIcon(":/images/dir.png"));
    rootItem->setData(true, Qt::UserRole + 1);
    invisibleItem->appendRow(rootItem);
    createChildrenItemsFromPro(".", "auto.pro", rootItem);

    return model;
}

/**************************************************************************************************
 **************************************************************************************************/
void QtDirectory::createChildrenItemsFromPro(const QString &dir, const QString &pro, QStandardItem *parent) const
{
    if (dir.isEmpty() || pro.isEmpty() || !parent)
        return;

    QFile f(repoTestSrcDir + "/" + dir + "/" + pro);
    if (!f.exists())
        return;

    QStandardItem *item;
    if (pro == "auto.pro") {
        item = parent;
    } else {
        QString name = pro;
        name.replace(".pro", "");
        item = new QStandardItem(name);
        item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
        item->setData(Qt::Unchecked, Qt::CheckStateRole);
        item->setData("/" + dir, Qt::UserRole);
        QString proFilePath = QFileInfo(f).absoluteFilePath();
        proFilePath.replace(repoTestSrcDir, "");
        item->setData(proFilePath, Qt::UserRole + 2);
        parent->appendRow(item);
    }

    QStringList subdirs = getSubdirsFromPro(f);
    QString tstName = pro;
    tstName.replace(".pro", ".cpp");
    QFileInfo srcFile(repoTestSrcDir + "/" + dir + "/tst_" + tstName);
    QFileInfo srcFile2(repoTestSrcDir + "/" + dir + "/test/tst_" + tstName);
    if (subdirs.isEmpty() || srcFile.exists() || srcFile2.exists()) {
        // it is an autotest
        item->setIcon(QIcon(":/images/test.png"));
        item->setData(false, Qt::UserRole + 1);

        if (srcFile2.exists())
            item->setData("/" + dir + "/test", Qt::UserRole);

    } else {
        // it is a directory
        item->setIcon(QIcon::fromTheme("folder", QIcon(":/images/dir.png")));
        item->setData(true, Qt::UserRole + 1);
        foreach (const QString &subdir, subdirs)
            if (subdir.endsWith(".pro"))
                createChildrenItemsFromPro(dir, subdir, item);
            else
                createChildrenItemsFromPro(dir + "/" + subdir, subdir + ".pro", item);
    }
}

/**************************************************************************************************
 **************************************************************************************************/
QStringList QtDirectory::getSubdirsFromPro(QFile &pro) const
{
    QStringList list;

    if (pro.open(QIODevice::ReadOnly | QIODevice::Text)) {
        bool inSubdirs = false;
        while (!pro.atEnd()) {
            QString line = pro.readLine();
            line = line.trimmed();
            if (!inSubdirs) {
                int comPos;
                if ((comPos = line.indexOf("#")) != -1)
                    line.truncate(comPos);
                if (line.contains("SUBDIRS +=") || line.contains("Q3SUBDIRS +=")
                    || line.contains("SUBDIRS =") || line.contains("Q3SUBDIRS =")
                    || line.contains("SUBDIRS+=") || line.contains("Q3SUBDIRS+=")
                    || line.contains("SUBDIRS=") || line.contains("Q3SUBDIRS=")) {
                    inSubdirs = true;
                    line.remove(0, line.indexOf("=") + 1);
                    if (!line.contains("\\"))
                        inSubdirs = false;
                    line.replace("\\", "");
                    line = line.trimmed();
                    QStringList sublist = line.split(" ", QString::SkipEmptyParts);
                    for (int i = 0; i < sublist.count(); i++) {
                        if (sublist.at(i).startsWith("$$")) {
                            sublist.removeAt(i);
                            i--;
                        }
                    }
                    list.append(sublist);
                }
            } else {
                if (line.isEmpty()) {
                    inSubdirs = false;
                } else {
                    if (!line.contains("\\"))
                        inSubdirs = false;
                    line.replace("\\", "");
                    int comPos;
                    if ((comPos = line.indexOf("#")) != -1)
                        line.truncate(comPos);
                    QStringList sublist = line.split(" ", QString::SkipEmptyParts);
                    for (int i = 0; i < sublist.count(); i++) {
                        if (sublist.at(i).startsWith("$$")) {
                            sublist.removeAt(i);
                            i--;
                        }
                    }
                    list.append(sublist);
                }
            }
        }
    }

    list.removeDuplicates();
    return list;
}

