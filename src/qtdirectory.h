/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Autotester project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TESTREPOSITORY_H
#define TESTREPOSITORY_H

#include <QtCore/QObject>
#include <QtCore/QHash>
#include <QtCore/QStringList>

class QStandardItemModel;
class QStandardItem;
class QFile;

class QtDirectory : public QObject
{
    Q_OBJECT

public:
    QtDirectory(const QString &name, const QString& location
#ifdef Q_WS_WIN
                , const QString &make
#endif
                );
    ~QtDirectory();

    inline QString name() const
    { return repoName; }
    inline QString buildDir() const
    { return repoBuildDir; }
    inline QString srcDir() const
    { return repoSrcDir; }
    inline QString libDir() const
    { return repoLibDir; }
    inline QString binDir() const
    { return repoBinDir; }
    inline QString testBuildDir() const
    { return repoTestBuildDir; }
    inline QString testSrcDir() const
    { return repoTestSrcDir; }
    inline QString qmake() const
    { return repoQmake; }
#ifdef Q_WS_WIN
    inline QString make() const
    { return repoMakeLocation; }
    inline QString makeDir() const
    { return repoMakeDir; }
#endif
    inline QStringList variants() const
    { return repoVariants; }

    QStandardItemModel *createModelFromPro() const;

    inline QString remoteOS() const
    { return repoRemoteOS; }
    inline bool isCrossCompiled() const
    { return repoCrossCompiled; }

private:
    QString repoName;
    QString repoQmake;
    QString repoBuildDir;
    QString repoSrcDir;
    QString repoLibDir;
    QString repoBinDir;
    QString repoTestBuildDir;
    QString repoTestSrcDir;
    bool repoCrossCompiled;
    QString repoRemoteOS;

#ifdef Q_WS_WIN
    QString repoMakeLocation;
    QString repoMakeDir;
#endif

    QStringList repoVariants;

    void createChildrenItemsFromPro(const QString &dir, const QString &pro, QStandardItem *parent) const;
    QStringList getSubdirsFromPro(QFile &pro) const;
};

#endif // TESTREPOSITORY_H
