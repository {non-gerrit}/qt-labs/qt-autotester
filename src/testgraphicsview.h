/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt Autotester project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef TESTGRAPHICSVIEW_H
#define TESTGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QGraphicsProxyWidget>

#define ZOOM_IN_FACTOR 1.18921
#define ZOOM_OUT_FACTOR 0.840896
#define ZOOM_MIN 0.29
#define ZOOM_MAX 2.4

class TestViewLayout;
class TestResultWidget;
class TestView;
class QStateMachine;
class QState;
class QGraphicsOpacityEffect;
class QGraphicsDropShadowEffect;

class TestGraphicsView : public QGraphicsView
{
    Q_OBJECT

public:
    TestGraphicsView(QWidget *parent = 0);
    ~TestGraphicsView();

    TestViewLayout *itemsLayout() const
    { return layout; }

    void showTestResultsDetail(TestView *t);
    void zoomIn();
    void zoomOut();
    void resetZoom();
    void zoomFit();

    inline qreal zoom() const
    { return currentZoomLevel; }
    void setZoom(qreal z);

Q_SIGNALS:
    void detailsRequested();
    void zoomLevelChanged(qreal zoomLevel);

protected:
    void wheelEvent(QWheelEvent *event);
    void resizeEvent(QResizeEvent *event);
    void adjustDetailsWidget();
    void updateLayout();

private:
    TestViewLayout *layout;
    QGraphicsWidget *testsContainer;
    TestResultWidget *widget;

    QStateMachine *machine;
    QState *detailsVisible;
    QState *detailsNotVisible;
    qreal currentZoomLevel;
};

#endif // TESTGRAPHICSVIEW_H
